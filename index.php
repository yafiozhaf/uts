<?php include("config.php"); ?>
<!doctype html>
<html lang="zxx">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tournament FF</title>
    <link rel="icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>
        <!--::header part start::-->
        <header class="main_menu">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="navbar-brand main_logo" href="index.html"> <img src="img/freefire.png" width="150px" alt="logo"> </a>
                            <a class="navbar-brand single_page_logo" href="index.html"> <img src="img/footer_logo.png" alt="logo"> </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                                <span class="menu_icon"></span>
                            </button>

                            <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="/">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="rules.html">Rules</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="prize.html">Info Prize</a>
                                    </li>
                                </ul>
                            </div>
                           
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!--::Header part end::-->

        <!--::banner part start::-->
        <section class="banner_part">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-5">
                        <div class="banner_img d-none d-lg-block">
                            <img src="img/banner_ff.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="banner_text">
                            <div class="banner_text_iner">
                                <h1>Go big or go home</h1>
                                <p>register yourself immediately to get a prize Rp. 5,000,000 </p>
                                <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> -->
                                <a href="registration.php" class="btn_2">REGISTRATION</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img src="img/animate_icon/Ellipse_7.png" alt="" class="feature_icon_1 custom-animation1">
            <img src="img/animate_icon/Ellipse_8.png" alt="" class="feature_icon_2 custom-animation2">
            <img src="img/animate_icon/Ellipse_1.png" alt="" class="feature_icon_3 custom-animation3">
            <img src="img/animate_icon/Ellipse_2.png" alt="" class="feature_icon_4 custom-animation4">
            <img src="img/animate_icon/Ellipse_3.png" alt="" class="feature_icon_5 custom-animation5">
            <img src="img/animate_icon/Ellipse_4.png" alt="" class="feature_icon_6 custom-animation6">
        </section>

        <!-- The Modal -->
    
        <!--::banner part start::-->


        <!--::about_us part start::-->
        <section class="review_part padding_bottom">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-6 col-lg-6">
                        <div class="review_img">
                            <img src="img/review_ff.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-5">
                        <div class="review_slider owl-carousel">
                            <div class="review_part_text">
                                <h2>With efficiency to
                                    unlock more opportunities</h2>
                                <p>Saw shall light. Us their to place had creepeth day
                                    night great wher appear to. Hath, called, sea called,
                                    gathering wherein open make living Female itself
                                    gathering man. Waters and, two. Bearing. Saw she'd
                                    all let she'd lights abundantly blessed.</p>
                                <h3>Japran, <span>CEO of ff</span> </h3>
                            </div>
                            <div class="review_part_text">
                                <h2>With efficiency to
                                    unlock more opportunities</h2>
                                <p>Saw shall light. Us their to place had creepeth day
                                    night great wher appear to. Hath, called, sea called,
                                    gathering wherein open make living Female itself
                                    gathering man. Waters and, two. Bearing. Saw she'd
                                    all let she'd lights abundantly blessed.</p>
                                <h3>Japran, <span>CEO of ff</span> </h3>
                            </div>
                            <div class="review_part_text">
                                <h2>With efficiency to
                                    unlock more opportunities</h2>
                                <p>Saw shall light. Us their to place had creepeth day
                                    night great wher appear to. Hath, called, sea called,
                                    gathering wherein open make living Female itself
                                    gathering man. Waters and, two. Bearing. Saw she'd
                                    all let she'd lights abundantly blessed.</p>
                                <h3>Japran, <span>CEO of ff</span> </h3>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <img src="img/animate_icon/Ellipse_4.png" alt="" class="feature_icon_2 custom-animation2">
        </section>
        <!--::about_us part end::-->

        <!--::subscribe us part end::-->
        <section class="subscribe_part padding_bottom">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="subscribe_part_text text-center">
                            <h2>Daftar Pemain</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Nomer Telepon</th>
                                            <th scope="col">ID</th>
                                            <th scope="col">Nama Akun</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $sql = "SELECT * FROM data";
                                        $row = $koneksi->prepare($sql);
                                        $row->execute();
                                        $hasil = $row->fetchAll();
                                        $a =1;
                                        foreach($hasil as $isi){
                                    ?>
                                        <tr>
                                            <td><?php echo $a ?></td>
                                            <td><?php echo $isi['nama']?></td>
                                            <td><?php echo $isi['phone']?></td>
                                            <td><?php echo $isi['id_ff']?></td>
                                            <td><?php echo $isi['namaAcc']?></td>
                                        </tr>
                                        <?php
                                            $a++;
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img src="img/animate_icon/Ellipse_5.png" alt="" class="feature_icon_2 custom-animation2">
        </section>
        <!--::subscribe us part end::-->

        <!--::client logo part end::-->
        <section class="client_logo">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="client_logo_slider owl-carousel d-flex justify-content-between">
                            <div class="single_client_logo">
                                <img src="img/client_logo/client_logo_1.png" alt="">
                            </div>
                            <div class="single_client_logo">
                                <img src="img/client_logo/client_logo_2.png" alt="">
                            </div>
                            <div class="single_client_logo">
                                <img src="img/client_logo/client_logo_3.png" alt="">
                            </div>
                            <div class="single_client_logo">
                                <img src="img/client_logo/client_logo_4.png" alt="">
                            </div>
                            <div class="single_client_logo">
                                <img src="img/client_logo/client_logo_5.png" alt="">
                            </div>
                            <div class="single_client_logo">
                                <img src="img/client_logo/client_logo_1.png" alt="">
                            </div>
                            <div class="single_client_logo">
                                <img src="img/client_logo/client_logo_2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--::client logo part end::-->

        <!--::footer_part start::-->
        <footer class="footer_part">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-4 col-lg-4">
                        <div class="single_footer_part">
                            <a href="/" class="footer_logo_iner"> <img src="img/freefire.png" width="150px" alt="#"> </a>
                            <p>Gathered. Under is whose you'll to make years is mat lights thing together fish made
                                forth thirds cattle behold won't. Fourth creeping first female.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-2">
                        <div class="single_footer_part">
                            <h4>About Us</h4>
                            <ul class="list-unstyled">
                                <li><a href="">Managed Website</a></li>
                                <li><a href="">Manage Reputation</a></li>
                                <li><a href="">Power Tools</a></li>
                                <li><a href="">Marketing Service</a></li>
                                <li><a href="">Customer Service</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-2">
                        <div class="single_footer_part">
                            <h4>Quick Links</h4>
                            <ul class="list-unstyled">
                                <li><a href="">Store Hours</a></li>
                                <li><a href="">Brand Assets</a></li>
                                <li><a href="">Investor Relations</a></li>
                                <li><a href="">Terms of Service</a></li>
                                <li><a href="">Privacy & Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-2">
                        <div class="single_footer_part">
                            <h4>My Account</h4>
                            <ul class="list-unstyled">
                                <li><a href="">Press Inquiries</a></li>
                                <li><a href="">Media Directories</a></li>
                                <li><a href="">Investor Relations</a></li>
                                <li><a href="">Terms of Service</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3 col-lg-2">
                        <div class="single_footer_part">
                            <h4>Resources</h4>
                            <ul class="list-unstyled">
                                <li><a href="">Application Security</a></li>
                                <li><a href="">Software Policy</a></li>
                                <li><a href="">Supply Chain</a></li>
                                <li><a href="">Agencies Relation</a></li>
                                <li><a href="">Manage Reputation</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="copyright_text">
                            <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="footer_icon social_icon">
                            <ul class="list-unstyled">
                                <li><a href="#" class="single_social_icon"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="single_social_icon"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="single_social_icon"><i class="fas fa-globe"></i></a></li>
                                <li><a href="#" class="single_social_icon"><i class="fab fa-behance"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--::footer_part end::-->

        <!-- jquery plugins here-->
        <script src="js/jquery-1.12.1.min.js"></script>
        <!-- popper js -->
        <script src="js/popper.min.js"></script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- easing js -->
        <script src="js/jquery.magnific-popup.js"></script>
        <!-- swiper js -->
        <script src="js/swiper.min.js"></script>
        <!-- swiper js -->
        <script src="js/masonry.pkgd.js"></script>
        <!-- particles js -->
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <!-- slick js -->
        <script src="js/slick.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/contact.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.form.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/mail-script.js"></script>
        <!-- custom js -->
        <script src="js/custom.js"></script>
    </body>
</html>